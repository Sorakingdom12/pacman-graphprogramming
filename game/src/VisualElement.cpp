#include "VisualElement.h"

VisualElement::VisualElement() 
{
	m_Frame = { 0,0,1,1 };
	m_Pivot = {0.5,0.5};
	m_Sprite = Texture2D();
}

VisualElement::VisualElement(Texture2D sprite, Vector2 position,Vector2 spriteSize) 
{
	m_Sprite = sprite;
	m_Frame = Rectangle{ position.x, position.y, spriteSize.x, spriteSize.y };
	m_Pivot = Vector2{ 0.5f,0.5f };
}

#pragma region GETTERS

Texture2D VisualElement::GetSprite() 
{
	return m_Sprite;
}

Rectangle VisualElement::GetRect()
{
	return m_Frame;
}

Vector2 VisualElement::GetSize()
{
	return Vector2{ m_Frame.width, m_Frame.height };
}

#pragma endregion

#pragma region SETTERS

void VisualElement::SetSprite(Texture2D sprite)
{
	m_Sprite = sprite;
}

void VisualElement::SetPivot(Vector2 newPivot)
{
	m_Pivot = newPivot;
}

void VisualElement::SetRotation(Vector2 direction)
{
}

void VisualElement::Draw(Vector2 position) {
	DrawTextureRec(m_Sprite,m_Frame, position, RAYWHITE);
}
#pragma endregion
