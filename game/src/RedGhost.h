#pragma once
#include <TilemapManager.h>
#include <VisualElement.h>
#include <raylib.h>

class RedGhost {
public:

	typedef enum Mode {
		CHASE,
		FRIGHTENED,
		DEAD
	};

	typedef enum State {
		RIGHT,
		LEFT,
		TOP,
		DOWN
	};

	VisualElement m_Sprite;
	int m_BaseAnimFrame = 0;
	int m_CurrentFrame = 0;
	int m_FrameCounter = 0;
	int m_FrameSpeed = 4;

	int m_frames;
	Mode m_Mode;
	Vector2 m_Position;
	float m_MovementSpeed = 80;
	State m_CurrentDirection;
	bool m_ReachedTarget;
	bool m_UseDoor;

	Vector2 m_TargetTilePosition;
	TilemapManager::Tile m_TargetTile;
	Vector2 m_DestinationPosition;

	RedGhost();
	void Reset(Vector2 position);
	void Init(TilemapManager::Tile startTile);
	void ChangeAnimDirection(State direction);
	void SetCurrentState(State direction);
	void SetDirection(Vector2 position);
	State GetOpositeDirection();
	void SetMode(Mode mode);
	void Update();
	void Draw();
};