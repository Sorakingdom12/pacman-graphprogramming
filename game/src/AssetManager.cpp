#include "AssetManager.h"

AssetManager::AssetManager() {
	
}

void AssetManager::Load() {
	m_Death = LoadSound("resources/Audio/Sounds/Death.mp3");
	m_EatGhost = LoadSound("resources/Audio/Sounds/EatingGhost.mp3");
	m_EatPill = LoadSound("resources/Audio/Sounds/EatingPowerPill.mp3");
	m_Start = LoadSound("resources/Audio/Sounds/GameStart.mp3");
	m_Victory = LoadSound("resources/Audio/Sounds/Victory.mp3");
	m_EatDot = LoadSound("resources/Audio/Sounds/WakaWaka.mp3");
	m_GhostSiren = LoadSound("resources/Audio/Sounds/Siren.mp3");
	m_Intro = LoadSound("resources/Audio/Music/GameOverTheme.mp3");
	m_GameOver = LoadSound("resources/Audio/Sounds/IntroTheme.mp3");
	m_LifeMarker = LoadTexture("resources/Game/IconLifes.png");
	m_GameLOGO = LoadTexture("resources/Menu/PacMan_MainLogo.png");
}

AssetManager* AssetManager::Instance;