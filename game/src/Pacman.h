#pragma once
#include "VisualElement.h"
#include "TilemapManager.h"

class Pacman
{
public:

	typedef enum State {
		RIGHT,
		LEFT,
		TOP,
		DOWN,
		DEAD
	};

	Vector2 m_Position;
	TilemapManager::Tile m_TargetTile;
	Vector2 m_TargetTileMapPosition;
	float m_MovementSpeed = 80;


	VisualElement m_MovementSprite;
	VisualElement m_DeadSprite;
	bool m_ReachedTarget;
	int m_CurrentFrame = 0;
	int m_FrameCounter = 0;
	int m_FrameSpeed = 4;
	State m_CurrentState;
	State m_DirectionBuffer;

	Pacman();
	TilemapManager::Tile GetTargetTile();
	void Init(TilemapManager::Tile position);
	State GetPlayerInput();
	void Update();
	void Draw();
};

