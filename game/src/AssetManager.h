#pragma once

#include <string>
#include "raylib.h"
using namespace std;

class AssetManager
{
public:

	Sound m_Death;
	Sound m_EatGhost;
	Sound m_EatPill;
	Sound m_Start;
	Sound m_Victory;
	Sound m_GhostSiren;
	Sound m_EatDot;
	Sound m_Intro;
	Sound m_GameOver;
	Texture2D m_LifeMarker;
	Texture2D m_GameLOGO;

	bool m_GameResult = false;
	int m_GameScore = 0;
	AssetManager();

	void Load();

	static AssetManager* Instance;

	static AssetManager* GetInstance() {
		if (Instance == nullptr) {
			Instance = new AssetManager();
		}
		return Instance; 
	};
};

