#include "RedGhost.h"
#include <iostream>
using namespace std;

RedGhost::RedGhost()
{

}

void RedGhost::Init(TilemapManager::Tile startTile)
{
	m_Position = startTile.position;
	m_TargetTile = startTile;
	m_TargetTilePosition = Vector2 { 13, 11 };
	m_CurrentDirection = TOP;
	m_Mode = CHASE;

	m_UseDoor = true;
	m_Sprite.m_Sprite = LoadTextureFromImage(LoadImage("resources/Enemies.png"));
	m_Sprite.m_Frame = Rectangle{ 0.0f, 0.0f, (float)m_Sprite.m_Sprite.width / 12,(float)m_Sprite.m_Sprite.height / 4 };
}

void RedGhost::Update()
{
	if (m_CurrentDirection == LEFT)
	{
		m_Position.x = std::max(m_Position.x - m_MovementSpeed * GetFrameTime(), m_TargetTile.position.x);
		m_ReachedTarget = m_Position.x == m_TargetTile.position.x;
	}
	else if (m_CurrentDirection == RIGHT)
	{
		m_Position.x = std::min(m_Position.x + m_MovementSpeed * GetFrameTime(), m_TargetTile.position.x);
		m_ReachedTarget = m_Position.x == m_TargetTile.position.x;
	}
	else if (m_CurrentDirection == TOP)
	{
		m_Position.y = std::max(m_Position.y - m_MovementSpeed * GetFrameTime(), m_TargetTile.position.y);
		m_ReachedTarget = m_Position.y == m_TargetTile.position.y;
	}
	else if (m_CurrentDirection == DOWN)
	{
		m_Position.y = std::min(m_Position.y + m_MovementSpeed * GetFrameTime(), m_TargetTile.position.y);
		m_ReachedTarget = m_Position.y == m_TargetTile.position.y;
	}
}

void RedGhost::ChangeAnimDirection(State direction) {
	if (m_Mode == CHASE) {
		if (m_CurrentDirection == TOP) {
			m_BaseAnimFrame = 4 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == LEFT) {
			m_BaseAnimFrame = 2 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == RIGHT) {
			m_BaseAnimFrame = 0 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == DOWN) {
			m_BaseAnimFrame = 6 * m_Sprite.m_Sprite.width / 12;
		}
	}
	else 
	{
		m_BaseAnimFrame = 8 * m_Sprite.m_Sprite.width / 12;
	}
}

void RedGhost::SetDirection(Vector2 position)
{
	if (position.x < m_TargetTilePosition.x)
		SetCurrentState(LEFT);
	else if (position.x > m_TargetTilePosition.x)
		SetCurrentState(RIGHT);
	else if (position.y > m_TargetTilePosition.y)
		SetCurrentState(DOWN);
	else if (position.y < m_TargetTilePosition.y)
		SetCurrentState(TOP);
	m_TargetTilePosition = position;
}

void RedGhost::SetMode(Mode mode)
{
	if (mode == CHASE) {
		m_Sprite.m_Frame.y = 0;
		m_Mode = CHASE;
		m_MovementSpeed = 80;
	}
	else if (mode == FRIGHTENED)
	{
		m_DestinationPosition = Vector2 { 26, 0 };
		m_Sprite.m_Frame.y = 0;
		m_Mode = FRIGHTENED;
		m_MovementSpeed = 48;
	}
	else {
		m_DestinationPosition = Vector2 { 13, 11 };
		m_Sprite.m_Frame.y = m_Sprite.m_Sprite.height / 4;
		m_Mode = DEAD;
		m_MovementSpeed = 160;
	}
	m_FrameCounter = 16;
	ChangeAnimDirection(m_CurrentDirection);
}

void RedGhost::Draw()
{
	m_FrameCounter++;
	if (m_Mode == CHASE) {
		if (m_FrameCounter >= (60 / m_FrameSpeed)) {
			m_FrameCounter = 0;
			m_CurrentFrame++;
			if (m_CurrentFrame > 1) m_CurrentFrame = 0;
			// Decide what sprite in the given direction
			m_Sprite.m_Frame.x = m_BaseAnimFrame + m_CurrentFrame * m_Sprite.m_Sprite.width / 12;
		}
	}
	else if (m_Mode == FRIGHTENED) {
		if (m_FrameCounter >= (60 / m_FrameSpeed)) {
			m_FrameCounter = 0;
			m_CurrentFrame++;
			if (m_CurrentFrame > 3) m_CurrentFrame = 0;
		}
		m_Sprite.m_Frame.x = m_BaseAnimFrame + m_CurrentFrame * m_Sprite.m_Sprite.width / 12;
	}
	else if (DEAD) 
	{
		m_FrameCounter = 0;
		m_CurrentFrame = 0;
		if (m_CurrentDirection == TOP) {
			m_Sprite.m_Frame.x = 10 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == DOWN) {
			m_Sprite.m_Frame.x = 11 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == RIGHT) {
			m_Sprite.m_Frame.x = 8 * m_Sprite.m_Sprite.width / 12;
		}
		else if (m_CurrentDirection == LEFT) {
			m_Sprite.m_Frame.x = 9 * m_Sprite.m_Sprite.width / 12;
		}
	}
	m_Sprite.Draw(m_Position);

}

RedGhost::State RedGhost::GetOpositeDirection() 
{
	if (m_CurrentDirection == TOP) 
		return DOWN;
	if (m_CurrentDirection == LEFT) 
		return RIGHT;
	if (m_CurrentDirection == RIGHT) 
		return LEFT;
	if (m_CurrentDirection == DOWN)
		return TOP;
}

void RedGhost::SetCurrentState(State direction) 
{
	m_CurrentDirection = direction;
	ChangeAnimDirection(direction);
}
