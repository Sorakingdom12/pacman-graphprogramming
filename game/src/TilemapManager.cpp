#include "TilemapManager.h"
#include <cmath>
TilemapManager::TilemapManager() {

}

// LESSON 05: Tilemap data loading and drawing
//----------------------------------------------------------------------------------
// Load tilemap data from file (text/image)
TilemapManager::Tilemap TilemapManager::LoadTilemap(const char* valuesMap, const char* collidersMap, const char* objectsMap)
{
    Tilemap map = { 0 };
    m_ConsumableCount = 0;
    const char* fileExt;

    if ((fileExt = strrchr(valuesMap, '.')) != NULL)
    {
        // Check if file extension is supported
        if (strcmp(fileExt, ".txt") == 0)
        {
            int counter = 0;
            int temp = 0;

            // Read values from text file
            FILE* valuesFile = fopen(valuesMap, "rt");

            while (!feof(valuesFile))
            {
                fscanf(valuesFile, "%i", &temp);
                counter++;
            }

            rewind(valuesFile);        // Return to the beginning of the file, to read again

            map.tiles = (Tile*)malloc(counter * sizeof(Tile));

            map.tileCountX = 27;
            map.tileCountY = 23;
            counter = 0;

            while (!feof(valuesFile))
            {
                fscanf(valuesFile, "%i", &map.tiles[counter].value);
                counter++;
            }

            fclose(valuesFile);

            // Read values from text file
            // NOTE: Colliders map data MUST match values data, 
            // or we need to do a previous check like done with values data
            FILE* collidersFile = fopen(collidersMap, "rt");
            counter = 0;
            temp = 0;

            while (!feof(collidersFile))
            {
                fscanf(collidersFile, "%i", &temp);
                map.tiles[counter].collider = temp;

                counter++;
            }

            fclose(collidersFile);
            
            FILE* objectsFile = fopen(objectsMap, "rt");
            counter = 0;
            temp = 0;

            while (!feof(objectsFile))
            {
                fscanf(objectsFile, "%i", &temp);
                if (temp != 1) {
                    map.tiles[counter].value = temp;
                    ++m_ConsumableCount;
                }
                counter++;
            }

            fclose(objectsFile);

            map.tileSize = 32;
            map.position = Vector2{ (float)GetScreenWidth() / 2 - map.tileCountX * map.tileSize / 2, (float)GetScreenHeight() / 2 - map.tileCountY * map.tileSize / 2 };

            for (int y = 0; y < map.tileCountY; y++)
            {
                for (int x = 0; x < map.tileCountX; x++)
                {
                    map.tiles[y * map.tileCountX + x].position = Vector2{ map.position.x + x * map.tileSize, map.position.y + y * map.tileSize };
                }
            }

#if DEBUG   // print tilemap information loaded
            for (int j = 0; j < map.tileCountY; j++)
            {
                for (int i = 0; i < map.tileCountX; i++)
                {
                    printf("%i ", map.tiles[j * map.tileCountX + i].collider);                    
                }

                printf("\n");
            }
#endif
        }
    }

    return map;
}

// Unload tilemap data from memory
void TilemapManager::UnloadTilemap(TilemapManager::Tilemap map)
{
    if (map.tiles != NULL) free(map.tiles);
}

// Draw tilemap using tileset
void TilemapManager::DrawTilemap(TilemapManager::Tilemap map, Texture2D tileset)
{
    for (int y = 0; y < map.tileCountY; y++)
    {
        for (int x = 0; x < map.tileCountX; x++)
        {
            // Draw each piece of the tileset in the right position to build map
            DrawTextureRec(tileset, tilesetRecs[map.tiles[y * map.tileCountX + x].value - 1],
                Vector2{ map.position.x + x * map.tileSize, map.position.y + y * map.tileSize
                }, WHITE);
        }
    }
}

bool TilemapManager::IsValidPosition(Vector2 position) 
{
    if (IsWarpGate(position))
        return true;
    if (position.x >= 0 && position.x < m_Tilemap.tileCountX && position.y >= 0 && position.y < m_Tilemap.tileCountY) {
        
        if (!m_Tilemap.tiles[(int)position.y * m_Tilemap.tileCountX + (int)position.x].collider)
            return true;
    }
    return false;
}

bool TilemapManager::AnyConsumablesLeft() {
    return m_ConsumableCount != 0;
}

void TilemapManager::ConsumeObject(Vector2 position) 
{
    m_Tilemap.tiles[(int)position.y * m_Tilemap.tileCountX + (int)position.x].value = 5;
    --m_ConsumableCount;
}

bool TilemapManager::HasObject(Vector2 position) 
{
    return m_Tilemap.tiles[(int)position.y * m_Tilemap.tileCountX + (int)position.x].value == 30 || m_Tilemap.tiles[(int)position.y * m_Tilemap.tileCountX + (int)position.x].value == 28;
}

TilemapManager::Tile TilemapManager::GetTile(Vector2 tilePosition) 
{
    return m_Tilemap.tiles[(int)tilePosition.y * m_Tilemap.tileCountX + (int)tilePosition.x];
}
bool TilemapManager::IsWarpGate(Vector2 position) {
    if ((position.x == m_WarpGateLeft.x && position.y == m_WarpGateLeft.y) || (position.x == m_WarpGateRight.x && position.y == m_WarpGateRight.y))
        return true;
    return false;
}