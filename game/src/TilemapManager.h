#pragma once
#include <raylib.h>
#include <stdio.h>              // Standard input-output C library
#include <stdlib.h>             // Memory management functions: malloc(), free()
#include <string.h>             // String manipulation functions: strrchr(), strcmp()

#define TILESET_TILES  32
    static Rectangle tilesetRecs[TILESET_TILES] = {
        { 0, 0, 32, 32 }, { 32, 0, 32, 32 },        // 1, 2
        { 64, 0, 32, 32 }, { 0, 32, 32, 32 },       // 3, 4
        { 32, 32, 32, 32 }, { 64, 32, 32, 32 },     // 5, 6
        { 0, 64, 32, 32 }, { 32, 64, 32, 32 },      // 7, 8
        { 64, 64, 32, 32 }, // 9

        { 96, 0, 32, 32 },  //10
        { 128, 0, 32, 32 }, { 160, 0, 32, 32 },     // 11, 12
        { 96, 32, 32, 32 }, { 128, 32, 32, 32 },    // 13, 14
        { 160, 32, 32, 32 }, { 96, 64, 32, 32 },   // 15, 16
        { 128, 64, 32, 32 }, { 160, 64, 32, 32 },      // 17, 18

        { 192, 0, 32, 32 }, { 192, 32, 32, 32 },     // 19, 20
        { 192, 64, 32, 32 },   // 21
        { 224, 0, 32, 32 },    // 22
        { 256, 0, 32, 32 }, { 288, 0, 32, 32 },   // 23, 24

        { 224, 32, 32, 32 }, { 256, 32, 32, 32 }, { 288, 32, 32, 32 },    // 25, 26, 27
        { 224, 64, 32, 32 },{ 256, 64, 32, 32 }, { 288, 64, 32, 32 },  // 28, 29, 30
    };

class TilemapManager
{

public:
    typedef struct Tile {
        int value;                  // Tile index value (in tileset)
        bool collider;              // Tile collider (0 -> walkable)
        Color color;                // Tile color (could be useful)
        Vector2 position;
    } Tile;

    // LESSON 05: Tilemap struct
    typedef struct Tilemap {
        Tile* tiles;                // Tiles data
        int tileCountX;             // Tiles counter X
        int tileCountY;             // Tiles counter Y
        int tileSize;              // Tile size (XY)
        Vector2 position;           // Tilemap position in screen
    } Tilemap;

    Tilemap m_Tilemap;
    Texture2D m_Tileset;
    int m_ConsumableCount;
    Vector2 m_WarpGateLeft = {0,11};
    Vector2 m_WarpGateRight = {26,11};

    TilemapManager();
    Tile GetTile(Vector2 tilePosition);
    bool IsWarpGate(Vector2 position);
    bool IsValidPosition(Vector2 position);
    bool HasObject(Vector2 position);
    void ConsumeObject(Vector2 position);
    bool AnyConsumablesLeft();
    Tilemap LoadTilemap(const char* valuesMap, const char* collidersMap, const char* objectsMap);// Load tilemap data from file
    void UnloadTilemap(Tilemap map);                   // Unload tilemap data
    void DrawTilemap(Tilemap map, Texture2D tileset);  // Draw tilemap using tileset
};

