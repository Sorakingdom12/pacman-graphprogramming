PACMAN PAC2

Esta es una versión mini del juego original que contiene un solo fantasma.

Se han implementado las siguientes features:

- Comportamiento de Blinky ( CHASE, FRIGHTEN, DEAD)
- Warp Tunnels
- Recolección de dots
- Pildoras power up 
- Comportamiento de PACMAN ( MOVEMENT, DEAD )
- Sistema de trackeo de colisiones por tilemap (paredes)
- Colisiones con el fantasma:
	En este caso he optado por calcular la colisión con este mediante la colisión entre circumferencias dada por la distancia entre ambos. De este modo si la distancia entre ellos es menor que la suma de los radios, hay colision.
- Movimiento:
	El sistema de movimiento que uso va en base a los tiles. Es decir, pacman no se mueve libremente sino que avanza linealmente en vertical u horizontal segun lo permitan los tiles. De este modo entre tile y tile se recoge un mini input buffer ara evitar que el giro sea frame perfect.
- Sonidos: Se han añadido todos los sonidos y efectos del juego. A excepción del sonido de intro (Muy estridente).
- Ending Screen: la pantalla final refleja el resultado de la partida y no solo derrota.
