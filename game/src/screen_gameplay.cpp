/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Gameplay Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include "raylib.h"
#include "screens.h"
#include "TilemapManager.h"//----------------------------------------------------------------------------------
#include "Pacman.h"
#include "RedGhost.h"
#include "vector"
#include <iostream>
using namespace std;
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = 0;

TilemapManager m_TilemapManager;
Pacman m_Player;
RedGhost m_Red;

int m_Score;
int m_Lifes;
float m_EmpoweredTime;
float m_DeathTimer;
float m_EndTimer;
bool m_GameEnded;
//----------------------------------------------------------------------------------
// Gameplay Screen Functions Definition
//----------------------------------------------------------------------------------


void MoveIfPosible(Pacman::State dir) {

    Vector2 current = m_Player.m_TargetTileMapPosition;
    if (dir == Pacman::LEFT)
    {
        Vector2 newTarget = Vector2{ current.x - 1,current.y };
        if (m_TilemapManager.IsValidPosition(newTarget)) {
            m_Player.m_CurrentState = dir;
            m_Player.m_TargetTile = m_TilemapManager.GetTile(newTarget);
            m_Player.m_TargetTileMapPosition = newTarget;
        }
    }
    else if (dir == Pacman::RIGHT)
    {
        Vector2 newTarget = Vector2{ current.x + 1,current.y };
        if (m_TilemapManager.IsValidPosition(newTarget))
        {
            m_Player.m_CurrentState = dir;
            m_Player.m_TargetTile = m_TilemapManager.GetTile(newTarget);
            m_Player.m_TargetTileMapPosition = newTarget;
        }
    }
    else if (dir == Pacman::TOP)
    {
        Vector2 newTarget = Vector2{ current.x ,current.y- 1 };
        if (m_TilemapManager.IsValidPosition(newTarget))
        {
            m_Player.m_CurrentState = dir;
            m_Player.m_TargetTile = m_TilemapManager.GetTile(newTarget);
            m_Player.m_TargetTileMapPosition = newTarget;
        }
    }
    else if (dir == Pacman::DOWN)
    {
        Vector2 newTarget = Vector2{ current.x ,current.y+ 1 };
        
        if (!(newTarget.x == 13 && newTarget.y == 10) && m_TilemapManager.IsValidPosition(newTarget))
        {
            m_Player.m_CurrentState = dir;
            m_Player.m_TargetTile = m_TilemapManager.GetTile(newTarget);
            m_Player.m_TargetTileMapPosition = newTarget;
        }
    }
}

bool HasReachedTarget(Vector2 currentPosition, Vector2 targetPosition) 
{
    return currentPosition.x == targetPosition.x && currentPosition.y == targetPosition.y;
}

vector<Vector2> SetAvailablePosition(Vector2 originTilePosition, RedGhost::State forbidden) 
{
    vector<Vector2> results;
    bool onlyTop = false;
    if (forbidden != RedGhost::TOP && m_TilemapManager.IsValidPosition(Vector2{ originTilePosition.x, originTilePosition.y - 1 })) {
        results.push_back(Vector2{ originTilePosition.x, originTilePosition.y - 1 });
        onlyTop = m_Red.m_UseDoor;
    }
    if(!onlyTop && forbidden != RedGhost::LEFT && m_TilemapManager.IsValidPosition(Vector2{ originTilePosition.x - 1, originTilePosition.y }))
        results.push_back(Vector2{ originTilePosition.x - 1, originTilePosition.y  });
    if(!onlyTop && forbidden != RedGhost::DOWN && m_TilemapManager.IsValidPosition(Vector2{ originTilePosition.x, originTilePosition.y + 1 }))
        results.push_back(Vector2{ originTilePosition.x, originTilePosition.y + 1 });
    if(!onlyTop && forbidden != RedGhost::RIGHT && m_TilemapManager.IsValidPosition(Vector2{ originTilePosition.x + 1, originTilePosition.y }))
        results.push_back(Vector2{ originTilePosition.x + 1, originTilePosition.y });
    if (onlyTop) m_Red.m_UseDoor = false;
    return results;
}

float GetDist(Vector2 first, Vector2 second) 
{
    return pow(second.x - first.x, 2) + pow(second.y - first.y, 2);
}

void SetNextRedGhostDirection()
{
    RedGhost::State forbidden = m_Red.GetOpositeDirection();
    vector<Vector2> availablePositions = SetAvailablePosition(m_Red.m_TargetTilePosition, forbidden);
    if (availablePositions.size() == 0) // Set forbidden direction
        m_Red.SetCurrentState(forbidden);
    else 
    {
        Vector2 targetPosition;
        if (m_Red.m_Mode == RedGhost::CHASE)
            targetPosition = m_Player.m_TargetTileMapPosition;
        else
            targetPosition = m_Red.m_DestinationPosition;
        
        int idx = -1;
        float minDist = 0;
        for (int i = 0; i < availablePositions.size(); ++i)
        {
            float dist = GetDist(availablePositions[i], targetPosition);
            if (idx == -1 || dist < minDist)
            {
                idx = i;
                minDist = dist;
            }
        }
        Vector2 chosen = availablePositions[idx];
        m_Red.SetDirection(chosen);
        m_Red.m_TargetTile = m_TilemapManager.GetTile(chosen);
    }
}

// Gameplay Screen Initialization logic
void InitGameplayScreen(void)
{
    // TODO: Initialize GAMEPLAY screen variables here!
    framesCounter = 0;
    finishScreen = 0;
    m_Score = 0;
    m_Lifes = 3;
    m_GameEnded = false;

    TilemapManager::Tilemap tilemap = m_TilemapManager.LoadTilemap("resources/tilemap.txt", "resources/tilemap_colliders.txt", "resources/tilemap_objects.txt");

    m_TilemapManager.m_Tilemap = tilemap;

    // LESSON 04: Load tileset texture
    Image imTileset = LoadImage("resources/tileset.png");
    Texture2D texTileset = LoadTextureFromImage(imTileset);
    UnloadImage(imTileset);
    m_TilemapManager.m_Tileset = texTileset;
    m_Player = Pacman();
    m_Red = RedGhost();
    m_Player.Init(m_TilemapManager.GetTile(Vector2{13, 17}));
    m_Red.Init(m_TilemapManager.GetTile(Vector2{ 13, 11 }));
    m_Red.m_frames = 0;
    PlaySound(AssetManager::GetInstance()->m_Start);
}

void AddPoints(int object) 
{
    if (object == 30) {
        m_Score += 10;
        PlaySoundMulti(AssetManager::GetInstance()->m_EatDot);
    }
    else if (object == 28) 
    {        
        PlaySoundMulti(AssetManager::GetInstance()->m_EatPill);
        m_Score += 50;
    }
    else
    {
        PlaySoundMulti(AssetManager::GetInstance()->m_EatGhost);
        m_Score += 200;
    }
}

void EndGame(bool result) 
{
    m_GameEnded = true;
    AssetManager::GetInstance()->m_GameScore = m_Score;
    if (result) {
        AssetManager::GetInstance()->m_GameResult = true;
        PlaySound(AssetManager::GetInstance()->m_Victory);
        m_EndTimer = 6;
    }
    else 
    {
        AssetManager::GetInstance()->m_GameResult = false;
        PlaySound(AssetManager::GetInstance()->m_GameOver);
        m_EndTimer = 0;
    }
}

void StartDeathTimer() 
{
    m_DeathTimer = 2;
    PlaySound(AssetManager::GetInstance()->m_Death);
    m_Player.m_CurrentState = Pacman::DEAD;
    m_Player.m_CurrentFrame = 0;
    m_Player.m_FrameCounter = 0;
    m_Player.m_FrameSpeed = 3;
    m_Lifes--;
    
}

void HandlePlayerDeath()
{
    //Play Death anim
    // Play Audio Death TODO
    if (m_DeathTimer > 0) {
        m_Player.Draw();
        m_DeathTimer -= GetFrameTime();
    }
    else {
        if (m_Lifes > 0) {
            Vector2 position = m_TilemapManager.GetTile(Vector2{ 13, 17 }).position;
            m_Player.Init(m_TilemapManager.GetTile(Vector2{ 13, 17 }));
            m_Red.Init(m_TilemapManager.GetTile(Vector2{ 13, 11 }));
        }
        else EndGame(false);
    }    
}

void EmpowerPacman() 
{
    m_EmpoweredTime = 10;
    if(m_Red.m_Mode == RedGhost::CHASE)
        m_Red.SetMode(RedGhost::FRIGHTENED);
}

void WarpPlayer() {
    if (m_Player.m_TargetTileMapPosition.x == m_TilemapManager.m_WarpGateLeft.x && m_Player.m_TargetTileMapPosition.y == m_TilemapManager.m_WarpGateLeft.y) { //enter from left EXIT right
        Vector2 exit = m_TilemapManager.GetTile(Vector2{ 26 ,11 }).position;
        m_Player.m_Position = {exit.x + m_TilemapManager.m_Tilemap.tileSize, exit.y};
        m_Player.m_TargetTileMapPosition = m_TilemapManager.m_WarpGateRight;
    }
    else //Enter from right Exit on LEFT
    {
        Vector2 exit = m_TilemapManager.GetTile(Vector2{ 0 ,11 }).position;
        m_Player.m_Position = { exit.x - m_TilemapManager.m_Tilemap.tileSize, exit.y };
        m_Player.m_TargetTileMapPosition = m_TilemapManager.m_WarpGateLeft;
    }
}

void WarpGhost() 
{
    if (m_Red.m_TargetTilePosition.x == m_TilemapManager.m_WarpGateLeft.x && m_Red.m_TargetTilePosition.y == m_TilemapManager.m_WarpGateLeft.y) { //enter from left EXIT right
        Vector2 exit = m_TilemapManager.GetTile(Vector2{ 26 ,11 }).position;
        m_Red.m_Position = { exit.x + m_TilemapManager.m_Tilemap.tileSize, exit.y };
        m_Red.m_TargetTilePosition = m_TilemapManager.m_WarpGateRight;
    }
    else //Enter from right Exit on LEFT
    {
        Vector2 exit = m_TilemapManager.GetTile(Vector2{ 0 ,11 }).position;
        m_Red.m_Position = { exit.x - m_TilemapManager.m_Tilemap.tileSize, exit.y };
        m_Red.m_TargetTilePosition = m_TilemapManager.m_WarpGateLeft;
    }
}

// Gameplay Screen Update logic
void UpdateGameplayScreen(void)
{
    if (m_GameEnded)
    {
        m_EndTimer -= GetFrameTime();
        if (m_EndTimer < 0)
            finishScreen = 1;
    }
    else {
        if (m_Player.m_CurrentState == Pacman::DEAD) {
            HandlePlayerDeath();
            return;
        }
        //Empower
        if (m_EmpoweredTime > 0) {
            m_EmpoweredTime -= GetFrameTime();
            m_EmpoweredTime = std::max(0.0f, m_EmpoweredTime);
            if (m_EmpoweredTime == 0)
                if (m_Red.m_Mode != RedGhost::DEAD)
                    m_Red.m_Mode = RedGhost::CHASE;
        }
        // Player movement logic
        Pacman::State DesiredDirection = m_Player.GetPlayerInput();
        if (m_Player.m_ReachedTarget)
        {
            //Check for consumables
            if (m_TilemapManager.HasObject(m_Player.m_TargetTileMapPosition)) {
                AddPoints(m_TilemapManager.GetTile(m_Player.m_TargetTileMapPosition).value);
                if (m_TilemapManager.GetTile(m_Player.m_TargetTileMapPosition).value == 28)
                    EmpowerPacman();
                m_TilemapManager.ConsumeObject(m_Player.m_TargetTileMapPosition);
            }
            //Check for warp
            if (m_TilemapManager.IsWarpGate(m_Player.m_TargetTileMapPosition))
                WarpPlayer();
            //move
            MoveIfPosible(DesiredDirection);
            if (m_Player.m_CurrentState != DesiredDirection)
            {
                MoveIfPosible(m_Player.m_CurrentState);
                m_Player.m_DirectionBuffer = m_Player.m_CurrentState;
            }

        }
        // Ghost movement logic
        if (m_Red.m_ReachedTarget)
        {
            if (m_Red.m_Mode == RedGhost::DEAD) {
                Vector2 dest = m_TilemapManager.GetTile(m_Red.m_DestinationPosition).position;
                if (m_Red.m_Position.x == dest.x && m_Red.m_Position.y == dest.y)
                {
                    m_Red.SetMode(RedGhost::CHASE);
                    m_Red.m_UseDoor = true;
                }
            }
            if (m_TilemapManager.IsWarpGate(m_Red.m_TargetTilePosition))
                WarpGhost();
            SetNextRedGhostDirection();
        }

        m_Player.Update();
        m_Red.Update();

        //Collision
        if (32 > std::sqrt(GetDist(m_Player.m_Position, m_Red.m_Position))) {
            if (m_Red.m_Mode == RedGhost::FRIGHTENED)
                m_Red.SetMode(RedGhost::DEAD);
            else if (m_Red.m_Mode != RedGhost::DEAD && m_Player.m_CurrentState != Pacman::DEAD) {
                StartDeathTimer();
            }
        }

        if (!m_TilemapManager.AnyConsumablesLeft()) {
            EndGame(true);
            return;
        }
    }
    if (IsKeyPressed(KEY_BACKSLASH)) finishScreen = 1;
}

// Gameplay Screen Draw logic
void DrawGameplayScreen(void)
{
    // TODO: Draw GAMEPLAY screen here!
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), BLACK);
    Vector2 pos = { 20, 10 };
    m_TilemapManager.DrawTilemap(m_TilemapManager.m_Tilemap,m_TilemapManager.m_Tileset);
    
    m_Player.Draw();
    m_Red.Draw();

    for (int i = 0; i < m_Lifes; i++) {
        DrawTexture(AssetManager::GetInstance()->m_LifeMarker, GetScreenWidth() - 50 - i * AssetManager::GetInstance()->m_LifeMarker.width, 10, RAYWHITE);
    }

    char score[20];
    sprintf(score, "SCORE: %d",m_Score);
    DrawTextEx(font, score, { 50,10 }, 20, 4, RAYWHITE);
}

// Gameplay Screen Unload logic
void UnloadGameplayScreen(void)
{
    // TODO: Unload GAMEPLAY screen variables here!
}

// Gameplay Screen should finish?
int FinishGameplayScreen(void)
{
    return finishScreen;
}