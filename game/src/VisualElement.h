#pragma once
#include <string>
#include "raylib.h"
using namespace std;

class VisualElement {

public:
	Texture2D m_Sprite;
	Rectangle m_Frame;
	Vector2 m_Pivot;

	VisualElement();
	VisualElement(Texture2D sprite, Vector2 position, Vector2 spriteSize);
#pragma region GETTERS

	Texture2D GetSprite();
	Rectangle GetRect();
	Vector2 GetSize();

#pragma endregion

#pragma region SETTERS

	void SetSprite(Texture2D sprite);
	void SetPivot(Vector2 newPivot);

#pragma endregion

	void SetRotation(Vector2 direction);

	void Draw(Vector2 position);


};