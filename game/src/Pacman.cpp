#include "Pacman.h"

Pacman::Pacman()
{	
}

void Pacman::Init(TilemapManager::Tile tile) 
{
	m_Position = tile.position;
	m_TargetTile = tile;
	m_TargetTileMapPosition = Vector2{13,17};
	m_ReachedTarget = false;
	m_CurrentState = RIGHT;
	m_MovementSprite.m_Sprite = LoadTextureFromImage(LoadImage("resources/PacMan.png"));
	m_MovementSprite.m_Frame = Rectangle{ 0.0f, 0.0f, (float)m_MovementSprite.m_Sprite.width / 2, (float)m_MovementSprite.m_Sprite.height / 4 };

	m_DeadSprite.m_Sprite = LoadTextureFromImage(LoadImage("resources/PacManDead.png"));
	m_DeadSprite.m_Frame = Rectangle{ 0.0f, 0.0f, (float)m_DeadSprite.m_Sprite.width / 12, (float)m_DeadSprite.m_Sprite.height };
}

void Pacman::Update()
{
	if(m_CurrentState == LEFT)
	{
		m_Position.x = std::max(m_Position.x - m_MovementSpeed * GetFrameTime(), m_TargetTile.position.x);
		m_ReachedTarget = m_Position.x == m_TargetTile.position.x;
	} 
	else if (m_CurrentState == RIGHT)
	{
		m_Position.x = std::min(m_Position.x + m_MovementSpeed * GetFrameTime(), m_TargetTile.position.x);
		m_ReachedTarget = m_Position.x == m_TargetTile.position.x;
	}
	else if (m_CurrentState == TOP)
	{
		m_Position.y = std::max(m_Position.y - m_MovementSpeed * GetFrameTime(), m_TargetTile.position.y);
		m_ReachedTarget = m_Position.y == m_TargetTile.position.y;
	}
	else if (m_CurrentState == DOWN)
	{
		m_Position.y = std::min(m_Position.y + m_MovementSpeed * GetFrameTime(), m_TargetTile.position.y);
		m_ReachedTarget = m_Position.y == m_TargetTile.position.y;
	}
}

void Pacman::Draw()
{
	m_FrameCounter++;
	if (m_CurrentState != DEAD) {

		if (m_FrameCounter >= (60 / m_FrameSpeed)) {
			m_FrameCounter = 0;
			m_CurrentFrame++;
			if (m_CurrentFrame > 1) m_CurrentFrame = 0;
			// Decide what sprite in the given direction
			m_MovementSprite.m_Frame.x = m_CurrentFrame * m_MovementSprite.m_Sprite.width / 2;
		}
		if (m_CurrentState == LEFT)	
			m_MovementSprite.m_Frame.y = 1 * m_MovementSprite.m_Sprite.height / 4;
		else if (m_CurrentState == RIGHT)
			m_MovementSprite.m_Frame.y = 0 * m_MovementSprite.m_Sprite.height / 4;
		else if (m_CurrentState == TOP)
			m_MovementSprite.m_Frame.y = 2 * m_MovementSprite.m_Sprite.height / 4;
		else if (m_CurrentState == DOWN)
			m_MovementSprite.m_Frame.y = 3 * m_MovementSprite.m_Sprite.height / 4;
		m_MovementSprite.Draw(m_Position);
	}
	else {
		
		if (m_FrameCounter >= (60 / m_FrameSpeed)) {
			m_FrameCounter = 0;
			m_CurrentFrame++;
			if (m_CurrentFrame > 11) m_CurrentFrame = 0;
			// Decide what sprite in the given direction
			m_DeadSprite.m_Frame.x = m_CurrentFrame * m_DeadSprite.m_Sprite.width / 12;
		}	
		
		m_DeadSprite.Draw(m_Position);
	}
}

TilemapManager::Tile Pacman::GetTargetTile() 
{
	return m_TargetTile;
}

Pacman::State Pacman::GetPlayerInput() {

	if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
		m_DirectionBuffer = LEFT;
	else if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
		m_DirectionBuffer = RIGHT;
	else if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP))
		m_DirectionBuffer = TOP;
	else if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN))
		m_DirectionBuffer = DOWN;
	return m_DirectionBuffer;
}